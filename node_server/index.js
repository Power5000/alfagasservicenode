// import fs from 'fs'
// console.log(fs)
//
// const value  = 24
//
// const mifn = num => num * 10
//
// console.log(mifn(value))

var http = require('http'),
    path = require('path'),
    fs   = require('fs'),
    host = '192.168.1.72',
    port = '9000';
const pathBaseProject = path.join(__dirname, '../')
// const loadFile = pathBaseProject+"alfagasservicenode/build/index.html";
const loadFile = pathBaseProject+"src/index.html";

var servidor = http.createServer((request,response)=>{
    const map = {
      '.ico' : 'image/x-icon',
      '.html': 'text/html',
      '.js'  : 'text/javascript',
      '.json': 'application/json',
      '.css' : 'text/css',
      '.png' : 'image/png',
      '.jpg' : 'image/jpeg',
      '.gif' : 'image/gif',
      '.wav' : 'audio/wav',
      '.mp3' : 'audio/mpeg',
      '.svg' : 'image/svg+xml',
      '.pdf' : 'application/pdf',
      '.doc' : 'application/msword'
    };
    var contentType = 'text/html';
    var chartCoding = 'UTF-8';
    var status      = 200;
    fs.readFile(loadFile,(err,data)=>{
        console.log(">>>>> err",err);
        console.log("request.method.toLowerCase()",request.method.toLowerCase());
        console.log(">>>>> request.url",request.url);
        if(request.url === '/'){
            fs.readFile(loadFile,(err,data)=>{
                if(err){
                    response.writeHead(404, {'Content-Type': 'text/html'});
                    response.write("Error Page");
                }
                else{
                    response.writeHead(200, {'Content-Type': 'text/html'});
                    response.write(data);
                }
                response.end();
            })
        }
        // else if(request.url === '/hola'){
        //     console.log(ruta 2);
        // }
        else if(request.url.match(".*\.")){
            let localPath  = request.url.replace(/.*\./gi,'');
            let sourcePath = map['.'+localPath];
            sourcePath     = !!sourcePath ? sourcePath : `text/${localPath}`;
            console.log("sourcePath",sourcePath)
            var cssPath = path.join(__dirname,'../src',request.url);
            // var cssPath = path.join(__dirname,'../alfagasservicenode/build',request.url);
            // fs.exists(sourcePath, function (exist) {
            //     console.log(">>>> exist",exist);
            //     if(!exist) {
            //       // if the file is not found, return 404
            //       response.writeHead(404, {'Content-Type': 'text/html'});
            //       response.write("Error Page");
            //       response.end();
            //       return;
            //     }
            //     var fileStream = fs.createReadStream(cssPath);
            //     response.writeHead(200,{'Content-Type': sourcePath});
            //     fileStream.pipe(response);
            // });

            var fileStream = fs.createReadStream(cssPath);
            response.writeHead(200,{'Content-Type': sourcePath});
            fileStream.pipe(response);
            // var cssPath = path.join(__dirname,'../src',request.url);
        }
    })
});

function init(){
    servidor.listen(port,()=>{
        console.log("start server listen port:"+port);
    })
}

init();
